<?php
/**
 *  Copyright 2011 Wordnik, Inc.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

/**
 *
 * NOTE: This class is auto generated by the swagger code generator program. Do not edit the class manually.
 */
class PostitsApi {

	function __construct($apiClient) {
	  $this->apiClient = $apiClient;
	}

  /**
	 * list
	 * List existing PostIts
   * @return MultiplePostItResponse
	 */

   public function list() {

  		//parse inputs
  		$resourcePath = "/postits/2.0/";
  		$resourcePath = str_replace("{format}", "json", $resourcePath);
  		$method = "GET";
      $queryParams = array();
      $headerParams = array();

      //make the API Call
      if (! isset($body)) {
        $body = null;
      }
  		$response = $this->apiClient->callAPI($resourcePath, $method,
  		                                      $queryParams, $body,
  		                                      $headerParams);


      if(! $response){
          return null;
        }

  		$responseObject = $this->apiClient->deserialize($response,
  		                                                'MultiplePostItResponse');
  		return $responseObject;

      }
  /**
	 * create
	 * Create a new PostIt
   * username, string: The username of a valid API user for whom this token will be generated. Default: the authenticated user (optional)
   * internalUsername, string: The username of a valid internal user to associate with this token. Default: empty (optional)
   * lifetime, int: The number of seconds this token should remain valid. Default: 2592000 (30 days) (optional)
   * maxUses, int: The maximum number of times this token can be used. Default: 1 (optional)
   * url, string: The target URL this PostIt should invoke. (optional)
   * noauth, bool: Whether this the target URL should be called without authentication. Default: false (optional)
   * @return PostIt
	 */

   public function create($username=null, $internalUsername=null, $lifetime=null, $maxUses=null, $url=null, $noauth=null) {

  		//parse inputs
  		$resourcePath = "/postits/2.0/";
  		$resourcePath = str_replace("{format}", "json", $resourcePath);
  		$method = "POST";
      $queryParams = array();
      $headerParams = array();

      //make the API Call
      if (! isset($body)) {
        $body = null;
      }
  		$response = $this->apiClient->callAPI($resourcePath, $method,
  		                                      $queryParams, $body,
  		                                      $headerParams);


      if(! $response){
          return null;
        }

  		$responseObject = $this->apiClient->deserialize($response,
  		                                                'PostIt');
  		return $responseObject;

      }
  /**
	 * delete
	 * Immediately invalidates this PostIt URL.
   * nonce, string: The nonce of this PostIt URL (required)
   * @return SinglePostItResponse
	 */

   public function delete($nonce) {

  		//parse inputs
  		$resourcePath = "/postits/2.0/{nonce}";
  		$resourcePath = str_replace("{format}", "json", $resourcePath);
  		$method = "DELETE";
      $queryParams = array();
      $headerParams = array();

      if($nonce != null) {
  			$resourcePath = str_replace("{" . "nonce" . "}",
  			                            $this->apiClient->toPathValue($nonce), $resourcePath);
  		}
  		//make the API Call
      if (! isset($body)) {
        $body = null;
      }
  		$response = $this->apiClient->callAPI($resourcePath, $method,
  		                                      $queryParams, $body,
  		                                      $headerParams);


      if(! $response){
          return null;
        }

  		$responseObject = $this->apiClient->deserialize($response,
  		                                                'SinglePostItResponse');
  		return $responseObject;

      }
  
}

