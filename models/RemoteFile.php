<?php
/**
 *  Copyright 2011 Wordnik, Inc.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

/**
 * $model.description$
 *
 * NOTE: This class is auto generated by the swagger code generator program. Do not edit the class manually.
 *
 */
class RemoteFile {

  static $swaggerTypes = array(
      'format' => 'string',
      'lastModified' => 'DateTime',
      'length' => 'int',
      'mimeType' => 'string',
      'name' => 'string',
      'owner' => 'string',
      'path' => 'string',
      'permissions' => 'string',
      'type' => 'string'

    );

  /**
  * The file type of the file.
  */
  public $format; // string
  /**
  * The date this file was last modified in ISO 8601 format.
  */
  public $lastModified; // DateTime
  /**
  * The length of the file/folder.
  */
  public $length; // int
  /**
  * The mime type of the file/folder. If unknown, it defaults to application/binary.
  */
  public $mimeType; // string
  /**
  * The name of the file/folder.
  */
  public $name; // string
  /**
  * Local username of the owner.
  */
  public $owner; // string
  /**
  * The absolute path to the file/folder.
  */
  public $path; // string
  /**
  * The permission of the invoking user on the file/folder.
  */
  public $permissions; // string
  /**
  * Whether it is a file or folder.
  */
  public $type; // string
  }

