<?php
/**
 *  Copyright 2011 Wordnik, Inc.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

/**
 * $model.description$
 *
 * NOTE: This class is auto generated by the swagger code generator program. Do not edit the class manually.
 *
 */
class ApplicationInputDetails {

  static $swaggerTypes = array(
      'description' => 'string',
      'label' => 'string',
      'argument' => 'string',
      'showArgument' => 'bool'

    );

  /**
  * Description of this input.
  */
  public $description; // string
  /**
  * The label for this input
  */
  public $label; // string
  /**
  * The command line value of this input (ex -n, --name, -name, etc)
  */
  public $argument; // string
  /**
  * Whether the argument value should be passed into the wrapper at run time
  */
  public $showArgument; // bool
  }

