<?php
/**
 *  Copyright 2011 Wordnik, Inc.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

/**
 * $model.description$
 *
 * NOTE: This class is auto generated by the swagger code generator program. Do not edit the class manually.
 *
 */
class ApplicationParameter {

  static $swaggerTypes = array(
      'details' => 'ApplicationParameterDetails',
      'id' => 'string',
      'semantics' => 'ApplicationParameterOntology',
      'value' => 'ApplicationParameterValue'

    );

  /**
  * The details for this parameter. 
  */
  public $details; // ApplicationParameterDetails
  /**
  * The id of this parameter. This will be the replacement string in your wrapper scripts.
  */
  public $id; // string
  /**
  * The ontologies for this parameter. 
  */
  public $semantics; // ApplicationParameterOntology
  /**
  * The inputs files for this parameter. 
  */
  public $value; // ApplicationParameterValue
  }

